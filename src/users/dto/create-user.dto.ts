import {IsNotEmpty, MinLength,IsPositive, Matches} from 'class-validator'
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
export class Product {
  id: number;

  @MinLength(8)
  name: string;

  @IsPositive()
  price: number; 
}
